
const ItemStyle = {
    item: {
        marginBottom: 10,
        paddingTop: 10,
        paddingBottom: 10,
        paddingRight: 15,
        paddingLeft: 15,
        backgroundColor: '#dddddd',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    title: {
        width: '60%',
        fontSize: 16,
        fontFamily: 'Roboto'
    },
    image: {
        width: '35%',
        height: 100
    }
};

export default ItemStyle;
import React from 'react';
import { View, Text, Image } from 'react-native';

import ItemStyle from './ItemStyle';

const Item = props => {
    return (
        props.info.map((key, index) => {
            return (
                <View style={ItemStyle.item} key={index}>
                    <Image style={ItemStyle.image} source={{uri : key.data.thumbnail}}/>
                    <Text style={ItemStyle.title}>{key.data.title}</Text>
                </View>
            )
        })
    )
};

export default Item;
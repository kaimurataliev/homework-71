
import * as actions from './actions';

const initialState = {
    data: []
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case actions.LOAD_INFO:
            return {...state, data: state.data.concat(action.data)};

        default:
            return state;
    }
};

export default reducer;
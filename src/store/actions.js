import axios from 'axios';


export const LOAD_INFO = "LOAD_INFO";
export const FETCH_ERROR = "FETCH_ERROR";

export const loadInfo = (data) => {
    return {type: LOAD_INFO, data};
};

export const fetchError = (error) => {
    return {type: FETCH_ERROR, error};
};


export const fetchData = () => {
    return (dispatch) => {
        axios.get('https://www.reddit.com/r/pics.json')
            .then(response => {
                const result = response.data.data.children;
                dispatch(loadInfo(result))
            }, error => {
                dispatch(fetchError(error))
            })
    }
};


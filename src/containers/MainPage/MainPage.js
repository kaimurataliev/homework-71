import React, { Component } from 'react';
import { ScrollView } from 'react-native';
import { connect } from 'react-redux';
import {fetchData} from "../../store/actions";

import MainStyle from './MainPageStyle';
import Item from '../../components/Item/Item';

class MainPage extends Component {

    componentDidMount() {
        this.props.fetchData();
    }

    render() {

        return (
            <ScrollView style={MainStyle.container}>
                <Item info={this.props.data}/>
            </ScrollView>

        )
    }
}

const mapStateToProps = state => {
    return {
        data: state.data
    }
};

const mapDispatchToProps = dispatch => {
    return {
        fetchData: () => dispatch(fetchData())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(MainPage);
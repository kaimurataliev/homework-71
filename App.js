import React from 'react';
import { Provider } from 'react-redux';
import reducer from './src/store/reducer';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { Font } from 'expo';

import MainPage from './src/containers/MainPage/MainPage';

const store = createStore(reducer, applyMiddleware(thunk));

export default class App extends React.Component {

    componentDidMount() {
        Font.loadAsync({
            'Roboto': require('./src/assets/fonts/Roboto-Medium.ttf'),
        });
    }

    render() {
        return (
            <Provider store={store}>
                <MainPage/>
            </Provider>
        )
    }
}


